from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import os

urlpatterns = patterns('',
                       # Examples:
                       url(r'^forum/', include('spirit.urls', namespace="spirit", app_name="spirit")),
                       url(r'', include('social_auth.urls')),
                       url(r'^$', 'index.views.index', name='index'),
                       url(r'^upload/$', 'index.views.upload', name='upload'),
                       url(r'^leaders/$', 'index.views.leaderboard', name='leaders'),
                       # url(r'^login/', 'index.views.login', name='login'),

                       url(r'^login/$', 'spirit.views.user.custom_login', {'template_name': 'auth/login.html', },
                           name='user-login'),
                       url(r'^logout/$', 'django.contrib.auth.views.logout', name='user-logout'),

                       url(r'^register/$', 'spirit.views.user.register', name='user-register'),


                       # url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'auth/login.html'},
                       # name="custom_login"),
                       # url(r'^register/', 'index.views.register', name='register'),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
                           {'document_root': os.path.join(os.path.dirname(__file__), 'static')}),
)
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
