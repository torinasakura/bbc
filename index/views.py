# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response, render
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from forms import Uploadform
from models import Submission


User = get_user_model()


def handle_uploaded_file(f, dest):
    destination = open(dest, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()


def index(request):
    c = {'user': request.user,
         'request': request}
    response = render_to_response('index.html', c)
    return response


@login_required
def upload(request):
    form = Uploadform(request.POST, request.FILES, request.user or None)
    c = {'user': request.user,
         'form': form,
         'request': request}
    if request.method == "POST":
        if form.is_valid():
            pform = form.save(commit=False)
            pform.user = request.user
            pform.score = 0
            pform.save()
            c['complete'] = True
            response = render(request, 'upload.html', c)
            return response
    response = render(request, 'upload.html', c)
    return response


def leaderboard(request):
    l = Submission.objects.all().order_by('-score')
    c = {'user': request.user,
         'leaders': l,
         'request': request}
    response = render_to_response('leaderboard.html', c)
    return response