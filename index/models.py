from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import AbstractUser
from spirit.models.user import AbstractForumUser
from core import settings


class User(AbstractUser, AbstractForumUser):
    class Meta:
        db_table = 'auth_user'


class Submission(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='User')
    file = models.FileField(upload_to='files', verbose_name='File')
    score = models.IntegerField(verbose_name='Score')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Create date')
    update_date = models.DateTimeField(auto_now=True, verbose_name='Update date')