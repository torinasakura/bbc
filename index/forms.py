from django import forms
from core import settings
from django.template.defaultfilters import filesizeformat
from models import Submission



class Uploadform(forms.ModelForm):
    def clean_file(self):
        file = self.cleaned_data['file']
        try:
            if file:
                file_type = file.content_type.split('/')[0]
                #print file_type

                if len(file.name.split('.')) == 1:
                    raise forms.ValidationError('File type is not supported', code='invalid')

                if file_type in settings.UPLOAD_FILE_TYPES:
                    if file._size > settings.UPLOAD_FILE_MAX_SIZE:
                        raise forms.ValidationError('Please keep filesize under %s. Current filesize %s' % (
                        filesizeformat(settings.UPLOAD_FILE_MAX_SIZE), filesizeformat(file._size)))
                else:
                    raise forms.ValidationError('File type is not supported', code='invalid')
        except:
            pass

        return file

    class Meta:
        model = Submission
        fields = ('file',)
