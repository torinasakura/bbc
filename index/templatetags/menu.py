from django import template
from index.models import Submission


register = template.Library()

@register.inclusion_tag('tt/menu.html')
def menu(path):
    l = Submission.objects.all().order_by('-score')[:10]
    return {'path': path,
            'leaderl': l}