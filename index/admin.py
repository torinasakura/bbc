from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext, ugettext_lazy as _
from .models import User, Submission


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'is_administrator', 'is_moderator',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

class SubmissionAdmin(admin.ModelAdmin):
    date_hierarchy = 'create_date'
    search_fields = ['user']
    list_display = ('user', 'score', 'create_date', 'file')

admin.site.register(Submission, SubmissionAdmin)

admin.site.register(User, CustomUserAdmin)
# admin.site.register(Submission, CustomSubmission)
