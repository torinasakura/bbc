import os
import random

from spirit.settings import *

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SECRET_KEY = 'n)ji#nn_kv^1^g2lrbjob3as*74lad%2t3!(=y@w^ct0r_l6kz'

DEBUG = True
# DEBUG = False


TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['blackboxchallenge.com']


# Application definition

INSTALLED_APPS = (
    'django_admin_bootstrapped.bootstrap3',
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'index',
    'spirit',
    'social_auth',
    'djconfig',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'djconfig.middleware.DjConfigLocMemMiddleware',
)

ROOT_URLCONF = 'urls'

WSGI_APPLICATION = 'core.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'djconfig': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-US'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
MEDIA_URL = '/media/'

AUTH_USER_MODEL = 'index.User'

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)


TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.static',
    'django.core.context_processors.csrf',
    'social_auth.context_processors.social_auth_by_name_backends',
    'django.contrib.messages.context_processors.messages',
)

SESSION_SERIALIZER='django.contrib.sessions.serializers.PickleSerializer'

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

TWITTER_CONSUMER_KEY = 'JrOa82RMtsPEPi4LeVlrPBRAU'
TWITTER_CONSUMER_SECRET = 'EVhRtCxopO8KDj7S4valL5AoSsLKcffgLqd3KInriLJXDUCXfj'
FACEBOOK_APP_ID = '819519091444416'
FACEBOOK_API_SECRET = '1559ad2aecb01212a0826d21276ded0d'
GOOGLE_OAUTH2_CLIENT_ID = '731632525671-noa30cjl7ch5db43inlh5806hmlva645.apps.googleusercontent.com'
GOOGLE_OAUTH2_CLIENT_SECRET = 'T6NCwsywYv1Pz6ooGWUPPMNR'
VK_APP_ID = '4653352'
VK_API_SECRET = 'QzjNwsvtWgxR3P1z0Hry'

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL = '/login-error/'

SOCIAL_AUTH_DEFAULT_USERNAME = lambda: random.choice(['Darth_Vader', 'Obi-Wan_Kenobi', 'R2-D2', 'C-3PO', 'Yoda'])

SOCIAL_AUTH_CREATE_USERS = True


UPLOAD_FILE_TYPES = ['zip', 'rar', ]
UPLOAD_FILE_MAX_SIZE = "1048576"

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST = '173.194.71.108'
EMAIL_HOST_USER = 'noreply@blackboxchallenge.com'
DEFAULT_FROM_EMAIL = 'noreply@blackboxchallenge.com'
EMAIL_HOST_PASSWORD = 'blackboxchallengeNoReply9_z'
EMAIL_PORT = 587
EMAIL_USE_TLS = True